package soap.ejb;

import java.util.Date;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
@XmlRootElement
public class Etudiant {

private String CIN ;
private String nom ;
private String prenom ;
private Date dateNaissance ;
private String adresse ;
private long numTel ;
private String email ;
public Etudiant() {
	super();
	// TODO Auto-generated constructor stub
}
public Etudiant(String cIN, String nom, String prenom, Date dateNaissance, String adresse, long numTel, String email) {
	super();
	CIN = cIN;
	this.nom = nom;
	this.prenom = prenom;
	this.dateNaissance = dateNaissance;
	this.adresse = adresse;
	this.numTel = numTel;
	this.email = email;
}

public String getCIN() {
	return CIN;
}
@XmlAttribute(name="CIN",required=true)
public void setCIN(String cIN) {
	CIN = cIN;
}
public String getNom() {
	return nom;
}
@XmlElement(name="nom")
public void setNom(String nom) {
	this.nom = nom;
}
public String getPrenom() {
	return prenom;
}
@XmlElement(name="prenom")
public void setPrenom(String prenom) {
	this.prenom = prenom;
}
public Date getDateNaissance() {
	return dateNaissance;
}
@XmlElement(name="dateNaissance")
public void setDateNaissance(Date dateNaissance) {
	this.dateNaissance = dateNaissance;
}
public String getAdresse() {
	return adresse;
}
@XmlElement(name="adresse")
public void setAdresse(String adresse) {
	this.adresse = adresse;
}
public long getNumTel() {
	return numTel;
}
@XmlElement(name="numTel")
public void setNumTel(long numTel) {
	this.numTel = numTel;
}
public String getEmail() {
	return email;
}
@XmlElement(name="email")
public void setEmail(String email) {
	this.email = email;
}



}
