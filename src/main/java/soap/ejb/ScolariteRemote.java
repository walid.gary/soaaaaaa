package soap.ejb;

import java.util.Date;
import java.util.List;

import javax.ejb.Remote;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
@WebService(name="ScolaritePort"/*binding name*/,targetNamespace="http://ws.scolarite.tn",
serviceName="ScolariteService"/*service name*/)//balise wsdl:service  
@Remote
public interface ScolariteRemote { // Dans PortType on trouve les methode  //Type on trouve les corps methodes et les types retour
@WebMethod
@WebResult(name ="etat")
public String getEtatInscreption();
@WebMethod
@WebResult(name ="fraisInscription")
public int calculerFraisInscription(@WebParam(name="numInscription")int numInscription);
@WebMethod
@WebResult(name ="infoEtudiant")
public Etudiant getInfoEtudiant(@WebParam(name="numInscreption")int numInscription);
@WebMethod
@WebResult
public List<Etudiant> getListeEtudiants();

@WebMethod
@WebResult(name="numInscription")
public int inscrire(@WebParam(name="cin")String cin,
		@WebParam(name="nom")String nom,
		@WebParam(name="prenom")String prenom,
		@WebParam(name="dateNaissance")Date dareNaiss,
		@WebParam(name="adresse")String adresse,
		@WebParam(name="numTel")long numtel,
		@WebParam(name="email")String email);


}
